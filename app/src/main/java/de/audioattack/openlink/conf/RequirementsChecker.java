package de.audioattack.openlink.conf;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.audioattack.openlink.IncognitoBrowser;
import de.audioattack.openlink.IncognitoBrowsers;

/**
 * Created by low012 on 20.10.17.
 */

public class RequirementsChecker {

    private static final String TAG = RequirementsChecker.class.getSimpleName();

    private static final List<IncognitoBrowser> PACKAGES = new ArrayList<>();

    static {
        PACKAGES.add(IncognitoBrowsers.FIREFOX);
        PACKAGES.add(IncognitoBrowsers.FENNEC);
        PACKAGES.add(IncognitoBrowsers.ICECAT);
        PACKAGES.add(IncognitoBrowsers.JELLY);
    }


    public static boolean supportsIncognitoMode(final PackageInfo packageInfo) {

        return supportsIncognitoMode(packageInfo.packageName, packageInfo.versionCode);
    }

    private static boolean supportsIncognitoMode(final String packageName, final int versionCode) {

        final Integer minVersionCode = getMinVersion(packageName);
        return minVersionCode != null && versionCode >= minVersionCode;
    }

    private static Integer getMinVersion(final String packageName) {

        for (final IncognitoBrowser browser : PACKAGES) {
            if (browser.packageName.equals(packageName)) {
                return browser.minVersionCode;
            }
        }

        return null;
    }

    public static boolean isIncognitoBowserInstalled(final Context context) {
        final PackageManager pm = context.getApplicationContext().getPackageManager();

        final List<PackageInfo> applications = pm.getInstalledPackages(PackageManager.GET_META_DATA);

        for (final PackageInfo applicationInfo : applications) {

            if (supportsIncognitoMode(applicationInfo.packageName, applicationInfo.versionCode)) {
                return true;
            }
        }

        return false;
    }

    public static IncognitoBrowser getIncognitoBrowser(final Context context) {
        final PackageManager pm = context.getApplicationContext().getPackageManager();

        final List<PackageInfo> applications = pm.getInstalledPackages(PackageManager.GET_META_DATA);

        for (final PackageInfo applicationInfo : applications) {

            if (supportsIncognitoMode(applicationInfo.packageName, applicationInfo.versionCode)) {
                return getIncognitoBrowser(applicationInfo.packageName);
            }
        }

        return null;
    }

    public static IncognitoBrowser getIncognitoBrowser(final String packageName) {

        for (final IncognitoBrowser browser : PACKAGES) {
            Log.d(TAG, packageName + " " + browser.packageName);

            if (browser.packageName.equals(packageName)) {
                return browser;
            }
        }

        return null;
    }


}



