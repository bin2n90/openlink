package de.audioattack.openlink;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import de.audioattack.openlink.conf.RequirementsChecker;

/**
 * Created by low012 on 23.10.17.
 */

public class IntentFactory {

    private static final String TAG = IntentFactory.class.getSimpleName();

    public static Intent createIntent(final Context context, final Uri uri, final boolean incognito) {

        final Intent intent;

        if (incognito) {

            final String scheme = uri.getScheme();

            if ("http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {

                final PackageInfo packageInfo = getAppWhichHandlesHttp(context);
                final boolean supported = (packageInfo != null && RequirementsChecker.supportsIncognitoMode(packageInfo));

                if (supported) {
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    final String extraName = RequirementsChecker.getIncognitoBrowser(packageInfo.packageName).incognitoExtra;
                    intent.putExtra(extraName, true);
                } else {
                    intent = getIntent(context, uri);
                }

            } else {
                intent = new Intent(Intent.ACTION_VIEW, uri);
            }

        } else {
            intent = new Intent(Intent.ACTION_VIEW, uri);
        }

        return intent;


    }

    private static Intent getIntent(final Context context, final Uri uri) {

        final Intent intent = new Intent(Intent.ACTION_VIEW);
        final IncognitoBrowser browser = RequirementsChecker.getIncognitoBrowser(context);
        intent.setClassName(browser.packageName, browser.browserActivityName);
        intent.putExtra(browser.incognitoExtra, true);
        intent.setData(uri);

        return intent;
    }

    public static PackageInfo getAppWhichHandlesHttp(final Context context) {

        final PackageManager packageManager = context.getPackageManager();

        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://example.org"));

        ComponentName info = intent.resolveActivity(packageManager);

        Log.d(TAG, info.getPackageName());

        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(info.getPackageName(), PackageManager.GET_META_DATA);
            Log.d(TAG, packageInfo.packageName + " " + packageInfo.versionCode);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return packageInfo;
    }


}
